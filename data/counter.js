const counters = [
  {
    name: "Counter 00",
    count: 0,
  },
  {
    name: "Counter 01",
    count: 1,
  },
  {
    name: "Counter 02",
    count: 2,
  }
];

module.exports = counters;
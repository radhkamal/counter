const counterModel = require("../models/counterModel");

const getCountersController = async (request, response) => {
  try {
    const counters = await counterModel.find();
    response.status(200).send(counters);
  } catch (error) {
    console.log(error);
  }
};

const addCounterController = async (request, response) => {
  try {
    const newCounter = new counterModel(request.body);
    await newCounter.save();
    console.log(newCounter);
    response.status(201).send("Counter created successfully!");
  } catch (error) {
    response.status(400).send(error);
    console.log(error);
  }
};

const editCounterController = async (request, response) => {
  try {
    await counterModel.findOneAndUpdate({
      _id: request.params.id,
    }, request.body);
    response.status(201).send("Product updated successfully!");
  } catch (error) {
    response.status(400).send(error);
    console.log(error);
  }
};

const deleteCounterController = async (request, response) => {
  try {
    await counterModel.findOneAndDelete({
      _id: request.params.id,
    });
    response.status(200).json("Product deleted successfully!");
  } catch (error) {
    response.status(400).send(error);
    console.log(error);
  }
};

module.exports = { getCountersController, addCounterController, editCounterController, deleteCounterController };


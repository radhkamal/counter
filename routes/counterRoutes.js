const express = require("express");
const { getCountersController, addCounterController, editCounterController, deleteCounterController } = require("../controllers/counterControllers");

const router = express.Router();

router.get("/getAll", getCountersController);
router.post("/create", addCounterController);
router.put("/edit/:id", editCounterController);
router.delete("/delete/:id", deleteCounterController);

module.exports = router;
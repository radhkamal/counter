const mongoose = require("mongoose");
const dotenv = require("dotenv");
const connectDB = require("./database/config");
const counterModel = require("./models/counterModel");
const counters = require("./data/counter.js");
require("colors");

// config
dotenv.config();

// seeder function
const importData = async () => {
  try {
    await connectDB();
    await counterModel.deleteMany();
    await counterModel.insertMany(counters);
    console.log("All products added!".bgGreen);
  } catch (error) {
    console.log(`${error}`.bgRed.inverse);
    process.exit(1);
  }
};

importData();
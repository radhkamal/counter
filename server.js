const express = require("express");
const morgan = require("morgan");
const bodyParser = require("body-parser");
const cors = require("cors");
const dotenv = require("dotenv");
require("colors");

// database config
const connectDB = require("./database/config");

// database connection
dotenv.config();
connectDB();

// main app
const app = express();

// middlewares
app.use(cors({
  credentials: true,
  origin: "*",
  methods: ["GET", "PUT", "POST", "DELETE"],
}));
app.use(express.json());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
  extended: false,
}));
app.use(morgan("dev"));

// routes
app.get("/", (request, response) => {
  response.send("API running on server...");
});

app.use("/api/counters", require("./routes/counterRoutes"));

// port
const PORT = process.env.port || 8080;

app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`.bgCyan.white);
});

